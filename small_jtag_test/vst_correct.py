#!/usr/bin/env python3

"""makes corrections to vst source from coriolis2 P&R
"""

import os
import sys

# run through all files
for fname in os.listdir("vst_src"):
    if not fname.endswith(".vst"):
        continue
    print (fname)
    # read the file
    fname = "vst_src/"+fname
    with open(fname) as f:
        txt = f.read()
    # replace vss / vdd : linkage bit with vss/vdd in bit
    txt = txt.replace("linkage bit", "in bit")
    # and double-underscores
    txt = txt.replace("__", "_")
    #fname = fname[:-3] + "vhdl"
    # write the file
    with open(fname, "w") as f:
        f.write(txt)
