#!/usr/bin/env python3
"""converts NIOLIB and NSXLIB from VBE into VHDL
"""

import os
import sys

# use the chroot to set up
# https://git.libre-soc.org/?p=dev-env-setup.git;a=blob;f=coriolis2-chroot;hb=HEAD
# reason for using the chroot: it's standardised across the ls180 project

VASY_CMD = "schroot -c coriolis -d /tmp -- ~/alliance/install/bin/vasy"
ALLIANCEBASE = "../alliance-check-toolkit/cells"
ALLIANCE_LIBS = ['nsxlib', 'niolib']

yosys_ghdl_template = "ghdl --std=08 -g %s\n"
yosys_ghdl_footer = """proc
write_verilog %s.v
write_ilang %s.il
"""

for libname in ALLIANCE_LIBS:

    NSXLIB = "%s/%s" % (ALLIANCEBASE, libname)

    os.system("mkdir -p %s" % libname)

    # yosys ghdl loader script
    ghdl_txt = "plugin -i ghdl\n"

    for fname in os.listdir(NSXLIB):
        if not fname.endswith(".vbe"):
            continue
        print (fname)
        prefix = fname[:-4] # strip ".vbe"
        os.system("cp %s/%s /tmp" % (NSXLIB, fname))
        os.system("rm -f /tmp/%s.vhd" % (prefix))
        os.system("%s -s -I vbe %s %s" % (VASY_CMD, fname, prefix))
        os.system("cp /tmp/%s.vhd %s" % (prefix, libname))
        # add loader template for this module
        if fname == 'sff1r_x4.vbe':
            # don't ask.
            #ghdl_txt += 'read_verilog freepdk_45/sff1r_x4.v\n'
            os.system("cp freepdk_45/%s.vhd %s" % (prefix, libname))
        ghdl_txt += yosys_ghdl_template % prefix

    ghdl_txt += yosys_ghdl_footer % (libname, libname)

    # write out the ghdl script
    fname = "%s/ghdl.ys" % libname
    print ("text to %s\n" % fname, ghdl_txt)
    with open(fname, "w") as f:
        f.write(ghdl_txt)
